$(document).ready(function () {
    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    // our map loaded from OSM
    const map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([285.78, 0.03]),
            zoom: 3
        })
    });

    //New style to change color of Stroke based upon number of zika cases
    var customStyleFunction = function(value) {

        let fill ='';
        // console.log(parseInt(value));
        // console.log(value);
        if(parseInt(value)=== 0) {
            fill = 'darkcyan';
        } else if(parseInt(value) <= 5) {
            fill = 'green';
        } else if(parseInt(value) <= 10) {
            fill = 'lightgreen';
        } else if(parseInt(value) <= 50) {
            fill = 'greenyellow';
        } else if(parseInt(value) <= 100) {
            fill = 'yellow';
        } else if(parseInt(value) <= 500) {
            fill = 'orange';
        } else if(parseInt(value) <= 1000) {
            fill = 'red';
        } else if(parseInt(value) <= 2000) {
            fill = 'firebrick';
        } else if(parseInt(value) <= 3000) {
            fill = 'maroon';
        } else if(parseInt(value) > 3000) {
            fill = 'black';
        }

        return new ol.style.Style({
                fill: new ol.style.Fill({
                    color: fill
                }),
                stroke: new ol.style.Stroke({color: 'black', width: 1})

            });
    };

    const reportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: 'http://localhost:8080/api/reports/2016-04-20'
    });
    const reportLayer = new ol.layer.Vector({
        source: reportSource,
        style: function (feature) {
            return customStyleFunction(feature.get('value'))
        }
    });
    map.addLayer(reportLayer);

    // Apparently this code was for Primary Objective 3....
    map.on('click', function (event) {
        $('#reports').empty();
        $('#location').empty();
        let array = [];
        map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
            console.log(feature);
            array.push(feature);
        });
        console.log(array);
        console.log(array.map(x => x.get('location')));
        let distinctLocation = [... new Set(array.map(x => x.get('location')))];
        let reportDate = (array.map(x => x.get('report_date')));
        let dataField = (array.map(x => x.get('data_field')));
        let value = (array.map(x => x.get('value')));
        let unit = (array.map(x => x.get('unit')));
        console.log(distinctLocation);
        console.log(reportDate);
        console.log(dataField);
        console.log(value);
        console.log(unit);

            //Create html container with the Location inside
            $('#location').append(`
            <li style="list-style-type:none"><h3>${distinctLocation}</h3>
            </li>`);

            //Loop through array elements to create list items for Date, Occurrence and Number
            for (let i = 0; i < reportDate.length; i++) {
            $('#reports').append(` 
            <li style="list-style-type:none"><p><Strong>Report Date:</Strong> ${reportDate[i]}</p> 
                <p><Strong>Report Type:</Strong> ${dataField[i]}</p> 
                <p><Strong>Count:</Strong> ${value[i]}</p>
                <hr>
            </li>`)
            }

    });

    var cursorHoverStyle = "pointer";
    var target = map.getTarget();

//target returned might be the DOM element or the ID of this element depending on how the map was initialized
//either way get a jQuery object for it
    var jTarget = typeof target === "string" ? $("#"+target) : $(target);

    map.on("pointermove", function (event) {
        var mouseCoordInMapPixels = [event.originalEvent.offsetX, event.originalEvent.offsetY];

        //detect feature at mouse coords
        var hit = map.forEachFeatureAtPixel(mouseCoordInMapPixels, function (feature, layer) {
            return true;
        });

        if (hit) {
            jTarget.css("cursor", cursorHoverStyle);
        } else {
            jTarget.css("cursor", "");
        }
    });

    let dropdown = $('#date-select');

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>--Please choose a Report Date--</option>');
    dropdown.prop('selectedIndex', 0);

    const url = 'http://localhost:8080/api/reports/dates';

    // Populate dropdown with list of report dates
    $.getJSON(url, function (data) {
        $.each(data, function (key, entry) {
            console.log(entry);
            console.log(entry.abbreviation);
            dropdown.append($('<option></option>').attr('value', entry).text(entry));
        })
    });

    $('#date-select').change(function() {
        reportLayer.setSource(new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://localhost:8080/api/reports/' + $('#date-select').val()
        }));
    });

    $('#search').click(function() {
        reportLayer.setSource(new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://localhost:8080/api/reports/search?q=' + $('#input').val()
        }));
    })

});
