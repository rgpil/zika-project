package org.launchcode.zikaDashboard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class IndexControllerTests {

    @Autowired
    private MockMvc mockMvc;

//    @Test
//    public void emptyTest() throws Exception {
//
//    }

    @Test
    public void indexPathWorks() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }
}
