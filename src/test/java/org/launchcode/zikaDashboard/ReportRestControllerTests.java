//package org.launchcode.zikaDashboard;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.launchcode.zikaDashboard.data.LocationRepository;
//import org.launchcode.zikaDashboard.data.ReportRepository;
//import org.launchcode.zikaDashboard.models.Report;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.hamcrest.Matchers.hasSize;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//
//@RunWith(SpringRunner.class)
//@IntegrationTestConfig
//public class ReportRestControllerTests extends AbstractBaseRestIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ReportRepository reportRepository;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    private Report testReport1;
//    private Report testReport2;
//
//    @Before
//    public void setup() {
//
//        testReport1 = new Report("2016-04-02","Brazil-Acre",
//                "state","zika_reported","BR0011",
//                "NA","NA","618","cases");
//        reportRepository.save(testReport1);
//
//        testReport2 = new Report("2016-08-04","Brazil-Acre",
//                "state","zika_reported","BR0011",
//                "NA","NA","500","cases");
//        reportRepository.save(testReport2);
//    }
//
//    @Test
//    public void testGetReports() throws Exception {
//        mockMvc.perform(get("/api/reports"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(contentType))
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].reportDate", is("2016-04-02")))
//                .andExpect(jsonPath("$[1].value", is("500")));
//    }
//
//    @Test
//    public void testGetReportByDate() throws Exception {
//        mockMvc.perform(get("/api/reports/2016-04-02", testReport1.getReportDate()))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(contentType))
//                .andExpect(jsonPath("$.features", hasSize(1)))
//                .andExpect(jsonPath("$.features[0].properties.report_date", is(testReport1.getReportDate())))
//                .andExpect(jsonPath("$.features[0].properties.location", is(testReport1.getLocation())));
//    }
//
//    @Test
//    public void testGetReportByDateNotFound() throws Exception {
//        mockMvc.perform(get("/api/reports/2016-45-01"))
//                .andExpect(status().isNotFound());
//    }
//}
