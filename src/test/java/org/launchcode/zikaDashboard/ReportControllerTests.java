//package org.launchcode.zikaDashboard;
//
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.launchcode.zikaDashboard.data.ReportRepository;
//import org.launchcode.zikaDashboard.features.WktHelper;
//import org.launchcode.zikaDashboard.models.Report;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@IntegrationTestConfig
//public class ReportControllerTests {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ReportRepository reportRepository;
//
////    @Before
////    public void setup(){
////        reportRepository.deleteAll();
////    }
////
////    @After
////    public void tearDown(){
////        reportRepository.deleteAll();
////    }
//
//    @Test
//    public void reportPathWorks() throws Exception {
//        this.mockMvc.perform(get("/report/")).andExpect(status().isOk());
//    }
//
//    @Test
//    public void testGetReports() throws Exception {
//        Report report = reportRepository.save(new Report("2016-04-02",
//                "Brazil-Acre", "state","zika_reported",
//                "BR0011", "NA","NA","618","cases"));
//        this.mockMvc.perform(get("/report/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features[0].properties.location_type",
//                        equalTo("state")));
//    }
//}
