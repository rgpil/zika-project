package org.launchcode.zikaDashboard.models;


import org.junit.Test;
import org.launchcode.zikaDashboard.features.WktHelper;

import static junit.framework.TestCase.assertEquals;


public class ReportTests {

    @Test
    public void testReportConstructor() {
        Report report = new Report("2016-04-02","Brazil-Acre",
                "state","zika_reported","BR0011",
                "NA","NA","618","cases");
                assertEquals("618", report.getValue());
        }
}
