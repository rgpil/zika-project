package org.launchcode.zikaDashboard;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepositoryTests {

    @Autowired
    private ReportRepository reportRepository;

    @Before
    public void setup() {
        reportRepository.deleteAll();
    }

    @After
    public void tearDown() {
        reportRepository.deleteAll();
    }

    @Test
    public void testGetAllReports() {
        Report reportOne = new Report();
        Report reportTwo = new Report();

        reportRepository.save(reportOne);
        reportRepository.save(reportTwo);

        assertEquals(2, reportRepository.count());
    }

    @Test
    public void testDataMatches() {
        Report report = new Report("2016-04-02","Brazil-Acre",
                "state","zika_reported","BR0011",
                "NA","NA","618","cases");
        TestCase.assertEquals("2016-04-02", report.getReportDate());
        TestCase.assertEquals("Brazil-Acre", report.getLocation());
        TestCase.assertEquals("state", report.getLocationType());
        TestCase.assertEquals("zika_reported", report.getDataField());
        TestCase.assertEquals("BR0011", report.getDataFieldCode());
        TestCase.assertEquals("NA", report.getTimePeriod());
        TestCase.assertEquals("NA", report.getTimePeriodType());
        TestCase.assertEquals("618", report.getValue());
        TestCase.assertEquals("cases", report.getUnit());

    }
}
