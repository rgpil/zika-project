BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

--COPY report(report_date, location, locationType, dataField, dataFieldCode, timePeriod, timePeriodType, value, unit) FROM '/tmp/all_reports-cleaned.csv' DELIMITER ','CSV HEADER;
COPY location(NAME_0, NAME_1, geom) FROM '/tmp/locations-cleaned.csv' DELIMITER ',' CSV HEADER;;

UPDATE location SET name_0_normalized = unaccent(name_0);
UPDATE location SET name_1_normalized = unaccent(name_1);

COMMIT;
