package org.launchcode.zikaDashboard.controllers;

import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Report;
import org.launchcode.zikaDashboard.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


//@Controller
//@RequestMapping("/report")
//public class ReportController {
//
//    @Autowired
//    private ReportRepository reportRepository;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    @RequestMapping(value = "/")
//    @ResponseBody
//    public FeatureCollection getReports(@RequestParam(name="id") Optional<String> Id)  {
//        List<Report> reports = new ArrayList<>();
//        if(Id.isPresent()) {
//            reports.add(reportRepository.findById(Integer.parseInt(Id.get())).get());
//        } else {
//            reports = reportRepository.findAll();
//        }
//
//
//        if(reports.isEmpty()) { return new FeatureCollection(); }
//
//        FeatureCollection featureCollection = new FeatureCollection();
//        for (Report report : reports) {
//            HashMap<String, Object> properties = new HashMap<>();
//            properties.put("report_date", report.getReportDate());
//            properties.put("location", report.getLocation());
//            properties.put("location_type", report.getLocationType());
//            properties.put("data_field", report.getDataField());
//            properties.put("data_field_code", report.getDataFieldCode());
//            properties.put("time_period", report.getTimePeriod());
//            properties.put("time_period_type", report.getTimePeriodType());
//            properties.put("value", report.getValue());
//            properties.put("unit", report.getUnit());
//            String country_name = report.getLocation().split("-")[0];
//            String state_name = report.getLocation().split("-")[1];
//            if(locationRepository.findByCountryName(country_name).size() > 0) {
//                featureCollection.addFeature(new Feature(locationRepository.findByCountryNameAndStateName(country_name, state_name).get(0).getGeom(), properties));
//            }
//
//        }
//        return featureCollection;
//    }
//}
