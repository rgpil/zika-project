package org.launchcode.zikaDashboard.controllers.es;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.launchcode.zikaDashboard.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping(value = "/api/reports")
public class    ReportDocumentController {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @GetMapping(value = "search")
    public ResponseEntity search(@RequestParam String q) {
        // create a NativeSearchQuery (with fuzzy search)
        NativeSearchQuery query = new NativeSearchQuery(QueryBuilders.fuzzyQuery("location", q));
        // append sort configuration
        query.addSort(new Sort(Sort.Direction.DESC, "reportDate"));


//        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("location", q);

        List<ReportDocument> results = new ArrayList<>();
        Iterator<ReportDocument> iterator = reportDocumentRepository.search(query).iterator();

        while(iterator.hasNext()) {
            results.add(iterator.next());
        }

//        if(results.isEmpty()) {
//            return new ResponseEntity(results, HttpStatus.OK); }

        FeatureCollection featureCollectionFuzzy = new FeatureCollection();
        for (ReportDocument report : results) {
            HashMap<String, Object> propertiesFuzzy = new HashMap<>();
            propertiesFuzzy.put("report_date", report.getReportDate());
            propertiesFuzzy.put("location", report.getLocation());
            propertiesFuzzy.put("location_type", report.getLocationType());
            propertiesFuzzy.put("data_field", report.getDataField());
            propertiesFuzzy.put("data_field_code", report.getDataFieldCode());
            propertiesFuzzy.put("time_period", report.getTimePeriod());
            propertiesFuzzy.put("time_period_type", report.getTimePeriodType());
            propertiesFuzzy.put("value", report.getValue());
            propertiesFuzzy.put("unit", report.getUnit());
            int stuff = report.getLocation().split("-").length;
            if(stuff > 1) {
                String country_name = report.getLocation().split("-")[0];
                String state_name = report.getLocation().split("-")[1];
                List<Location> locations = locationRepository.findByCountryNameAndStateName(country_name,state_name);

//                if(report.getLocation().split("-").length > 1) {
//                    String state_name = report.getLocation().split("-")[1];
//                    if (locationRepository.findByCountryName(country_name).size() > 0) {
                        if(locations.size() > 0) {
                            featureCollectionFuzzy.addFeature(new Feature(
                                    locations.get(0).getGeom(),
                                    propertiesFuzzy));
                        }
                    }
//                }
            }
//        }
        return new ResponseEntity(featureCollectionFuzzy, HttpStatus.OK);

    }

    @GetMapping(value = "/{report_date}")
    public ResponseEntity getReportDate(@PathVariable("report_date") String report_date) {
//        if(reportDocumentRepository.searchByReportDate(report_date, PageRequest.of(0, 250000))) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        else {
//            Page<ReportDocument> reports = reportDocumentRepository.searchByReportDate(report_date, PageRequest.of(0, 250000));
                List<Report> reports = reportRepository.findByReportDate(report_date);
            if(reports.isEmpty()) {
                return new ResponseEntity(reports, HttpStatus.OK); }

            FeatureCollection featureCollection = new FeatureCollection();
            for (Report report : reports) {
                HashMap<String, Object> properties = new HashMap<>();
                properties.put("report_date", report.getReportDate());
                properties.put("location", report.getLocation());
                properties.put("location_type", report.getLocationType());
                properties.put("data_field", report.getDataField());
                properties.put("data_field_code", report.getDataFieldCode());
                properties.put("time_period", report.getTimePeriod());
                properties.put("time_period_type", report.getTimePeriodType());
                properties.put("value", report.getValue());
                properties.put("unit", report.getUnit());
//                System.out.println(report.getLocation());
                int stuff = report.getLocation().split("-").length;
                if(stuff > 1) {
                    String country_name = report.getLocation().split("-")[0];
                    String state_name = report.getLocation().split("-")[1];
                    List<Location> locations = locationRepository.findByCountryNameAndStateName(country_name,state_name);

//                if(report.getLocation().split("-").length > 1) {
//                    String state_name = report.getLocation().split("-")[1];
//                    if (locationRepository.findByCountryName(country_name).size() > 0) {
                    if(locations.size() > 0) {
                        featureCollection.addFeature(new Feature(
                                locations.get(0).getGeom(),
                                properties));
                    }
                }
//                }
            }
//        }
            return new ResponseEntity(featureCollection, HttpStatus.OK);

        }
//    }

    @GetMapping(value = "/dates")
    public ResponseEntity getAllDistinctReportDates() {
        List<String> reports = reportRepository.findDistinctByReportDates();
        return new ResponseEntity(reports, HttpStatus.OK);
    }

    @PostMapping(value = "/assignStates")
    public ResponseEntity<String> assignStates() {

        List<Location> locations = locationRepository.findAll();

        for (Location location : locations) {
            String countryPart = location.getCountryName().replace(" ", "_");
            String statePart = location.getStateName().replace(" ", "_");
            List<Report> matchingReports = reportRepository.findByLocationStartingWithIgnoreCase(countryPart + "-" + statePart);
            for (Report report : matchingReports) {
                report.setState(location);
            }
            reportRepository.saveAll(matchingReports);
        }

        return new ResponseEntity<>("Complete", HttpStatus.OK);
    }
}

