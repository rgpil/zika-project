package org.launchcode.zikaDashboard.controllers.es;

import org.launchcode.zikaDashboard.utils.EsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/es")
public class EsController {

    @Autowired
    private EsUtil esUtil;

    @PostMapping(value = "/refresh")
    public ResponseEntity refresh() {
        esUtil.refresh();
        return new ResponseEntity("Refreshed Elasticsearch index\n", HttpStatus.OK);

    }

}

