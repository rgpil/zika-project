//package org.launchcode.zikaDashboard.controllers.rest;
//
//
//import org.launchcode.zikaDashboard.data.LocationRepository;
//import org.launchcode.zikaDashboard.data.ReportRepository;
//import org.launchcode.zikaDashboard.features.Feature;
//import org.launchcode.zikaDashboard.features.FeatureCollection;
//import org.launchcode.zikaDashboard.models.Report;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.HashMap;
//import java.util.List;
//
//@RestController
//@RequestMapping(value = "/api/reports")
//public class ReportRestController {
//
//    @Autowired
//    private ReportRepository reportRepository;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    @GetMapping
//    public ResponseEntity getReports() {
//        List<Report> reports = reportRepository.findAll();
//        if(reports.size() == 0) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity(
//                reports, HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/{report_date}")
//    public ResponseEntity getReportDate(@PathVariable("report_date") String report_date) {
//        if(reportRepository.findByReportDate(report_date).size() == 0) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        else {
//            List<Report> reports = reportRepository.findByReportDate(report_date);
//
//            if(reports.isEmpty()) {
//                return new ResponseEntity(reports, HttpStatus.OK); }
//
//                FeatureCollection featureCollection = new FeatureCollection();
//            for (Report report : reports) {
//                HashMap<String, Object> properties = new HashMap<>();
//                properties.put("report_date", report.getReportDate());
//                properties.put("location", report.getLocation());
//                properties.put("location_type", report.getLocationType());
//                properties.put("data_field", report.getDataField());
//                properties.put("data_field_code", report.getDataFieldCode());
//                properties.put("time_period", report.getTimePeriod());
//                properties.put("time_period_type", report.getTimePeriodType());
//                properties.put("value", report.getValue());
//                properties.put("unit", report.getUnit());
////                System.out.println(report.getLocation());
//                if(report.getLocation().length() > 0); {
//                    String country_name = report.getLocation().split("-")[0];
//                    if(report.getLocation().split("-").length > 1) {
//                        String state_name = report.getLocation().split("-")[1];
//                        if (locationRepository.findByCountryName(country_name).size() > 0) {
//                            if(locationRepository.findByCountryNameAndStateName(
//                                    country_name, state_name).size() > 0) {
//                                        featureCollection.addFeature(new Feature(
//                                            locationRepository.findByCountryNameAndStateName(
//                                                country_name, state_name).get(0).getGeom(),
//                                                properties));
//                            }
//                        }
//                    }
//                }
//            }
//            return new ResponseEntity(featureCollection, HttpStatus.OK);
//
//        }
//    }
//
//    @GetMapping(value = "/dates")
//    public ResponseEntity getAllDistinctReportDates() {
//        List<String> reports = reportRepository.findDistinctByReportDates();
//        return new ResponseEntity(reports, HttpStatus.OK);
//    }
//}
