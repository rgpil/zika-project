package org.launchcode.zikaDashboard.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_0")
    private String countryName;
//    private String name_0_normalized;
    @Column(name = "name_1")
    private String stateName;
//    private String name_1_normalized;
    private Geometry geom;

    public Location() {}

    public Location(String countryName, String stateName, Geometry geom)

    {
        this.countryName = countryName;
        this.stateName = stateName;
        this.geom = geom;
    }

    @OneToMany
    @JoinColumn(name = "state_id")
    private List<Report> reports = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public List<Report> getReports() { return reports; }

    public void setReports(List<Report> reports) { this.reports = reports; }

    //    public String getName_0_normalized() {
//        return name_0_normalized;
//    }
//
//    public void setName_0_normalized(String name_0_normalized) {
//        this.name_0_normalized = name_0_normalized;
//    }
//
//    public String getName_1_normalized() {
//        return name_1_normalized;
//    }
//
//    public void setName_1_normalized(String name_1_normalized) {
//        this.name_1_normalized = name_1_normalized;
//    }

}
