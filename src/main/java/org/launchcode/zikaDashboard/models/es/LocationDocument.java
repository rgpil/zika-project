//package org.launchcode.zikaDashboard.models.es;
//
//import com.vividsolutions.jts.geom.Geometry;
//import org.launchcode.zikaDashboard.models.Location;
//import org.springframework.data.elasticsearch.annotations.Document;
//
//import javax.persistence.Column;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//@Document(indexName = "#{esConfig.indexName}", type = "location")
//public class LocationDocument {
//
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    private String id;
//
//    @Column(name = "name_0")
//    private String countryName;
//    private String name_0_normalized;
//    @Column(name = "name_1")
//    private String stateName;
//    private String name_1_normalized;
//    private Geometry geom;
//
//    public LocationDocument() {}
//
//    public LocationDocument(Location location)
//
//    {
//        this.countryName = location.getCountryName();
//        this.stateName = location.getStateName();
//        this.geom = location.getGeom();
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getCountryName() {
//        return countryName;
//    }
//
//    public void setCountryName(String countryName) {
//        this.countryName = countryName;
//    }
//
//    public String getName_0_normalized() {
//        return name_0_normalized;
//    }
//
//    public void setName_0_normalized(String name_0_normalized) {
//        this.name_0_normalized = name_0_normalized;
//    }
//
//    public String getStateName() {
//        return stateName;
//    }
//
//    public void setStateName(String stateName) {
//        this.stateName = stateName;
//    }
//
//    public String getName_1_normalized() {
//        return name_1_normalized;
//    }
//
//    public void setName_1_normalized(String name_1_normalized) {
//        this.name_1_normalized = name_1_normalized;
//    }
//
//    public Geometry getGeom() {
//        return geom;
//    }
//
//    public void setGeom(Geometry geom) {
//        this.geom = geom;
//    }
//}
