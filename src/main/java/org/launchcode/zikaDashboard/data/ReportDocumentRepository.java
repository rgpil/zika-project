package org.launchcode.zikaDashboard.data;

import org.elasticsearch.index.query.QueryBuilder;
import org.hibernate.annotations.SortNatural;
import org.launchcode.zikaDashboard.models.Report;
import org.launchcode.zikaDashboard.models.es.ReportDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ReportDocumentRepository
        extends ElasticsearchRepository<ReportDocument, String> {

    Page<ReportDocument> search(SearchQuery queryBuilder);

    @Query("{ \"query\": {\"bool\": {\"must\": {\"match\": {\"reportDate\": \"?0\"}}}}, \"sort\": { \"reportDate\": { \"mode\": \"max\", \"order\": \"desc\"}}}")
    Page<ReportDocument> searchByReportDate(String reportDate, Pageable pageable);

//    List<ReportDocument> findByReportDate(String reportDate);
}
