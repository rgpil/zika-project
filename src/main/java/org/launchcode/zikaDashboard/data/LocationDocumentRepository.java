//package org.launchcode.zikaDashboard.data;
//
//import com.vividsolutions.jts.geom.Location;
//import org.elasticsearch.index.query.QueryBuilder;
//import org.launchcode.zikaDashboard.models.es.LocationDocument;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//
//import java.util.Collection;
//import java.util.List;
//
//public interface LocationDocumentRepository
//        extends ElasticsearchRepository<LocationDocument, String> {
//
//    Iterable<LocationDocument> search(QueryBuilder queryBuilder);
//
//    List<Location> findByCountryName(String country_name);
//
//    List<Location> findByCountryNameAndStateName(String country_name, String state_name);
//}
