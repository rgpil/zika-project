package org.launchcode.zikaDashboard.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.launchcode.zikaDashboard.models.Location;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findByCountryName(String location);

    List<Location> findByCountryNameAndStateName(String country_name, String state_name);

}
