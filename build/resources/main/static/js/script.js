$(document).ready(function () {
    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    // our map loaded from OSM
    const map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([285.78, 0.03]),
            zoom: 3
        })
    });

    //New style to change color of Stroke based upon number of zika cases
    var customStyleFunction = function(state_total) {

        let fill ='';
        // console.log(parseInt(value));
        // console.log(value);
        if(state_total === 0) {
            fill = 'darkcyan';
        } else if(state_total <= 5) {
            fill = 'green';
        } else if(state_total <= 10) {
            fill = 'lightgreen';
        } else if(state_total <= 50) {
            fill = 'greenyellow';
        } else if(state_total <= 100) {
            fill = 'yellow';
        } else if(state_total <= 500) {
            fill = 'orange';
        } else if(state_total <= 1000) {
            fill = 'orangered';
        } else if(state_total <= 2000) {
            fill = 'red';
        } else if(state_total <= 3000) {
            fill = 'firebrick';
        } else if(state_total > 3000) {
            fill = 'maroon';
        }

        return new ol.style.Style({
                fill: new ol.style.Fill({
                    color: fill
                }),
                stroke: new ol.style.Stroke({color: 'black', width: 1})

            });
    };

    const reportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        // url: '/api/reports/2016-04-20'
    });
    const reportLayer = new ol.layer.Vector({
        source: reportSource,
        style: function (feature) {
            return customStyleFunction(feature.get('state_total'))
        }
    });
    map.addLayer(reportLayer);

    // Apparently this code was for Primary Objective 3....
    map.on('click', function (event) {
        $('#reports').empty();
        $('#location').empty();
        let array = [];
        map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
            console.log(feature);
            array.push(feature);
        });
        let country = [... new Set(array.map(x => x.get('name_0')))];
        let distinctLocation = [... new Set(array.map(x => x.get('name_1')))];
        let reportDate = (array.map(x => x.get('report_date')));
        let dataField = (array.map(x => x.get('data_field')));
        let value = (array.map(x => x.get('cases')));
        let unit = (array.map(x => x.get('unit')));
        let stateTotal = [... new Set(array.map(x => x.get('state_total')))];

            //Create html container with the Location inside
            $('#location').append(`
            <li style="list-style-type:none"><h3>${country}</h3><h5><i>${distinctLocation}</i></h5><h7><Strong>Total Reports: </Strong>${stateTotal}</h7>
            <hr>
            </li>`);

            //Loop through array elements to create list items for Date, Occurrence and Number
            sum = 0;
            for (let i = 0; i < reportDate.length; i++) {
            $('#reports').append(` 
            <li style="list-style-type:none"><p><Strong>Report Date:</Strong> ${reportDate[i]}</p> 
                <p><Strong>Report Type:</Strong> ${dataField[i]}</p> 
                <p><Strong>Count:</Strong> ${value[i]}</p>
                <hr>
            </li>`)
                // sum += value[i];
            }
            // feature.add({'sum': sum});
            // console.log(sum);

    });

    var cursorHoverStyle = "pointer";
    var target = map.getTarget();

//target returned might be the DOM element or the ID of this element depending on how the map was initialized
//either way get a jQuery object for it
    var jTarget = typeof target === "string" ? $("#"+target) : $(target);

    map.on("pointermove", function (event) {
        var mouseCoordInMapPixels = [event.originalEvent.offsetX, event.originalEvent.offsetY];

        //detect feature at mouse coords
        var hit = map.forEachFeatureAtPixel(mouseCoordInMapPixels, function (feature, layer) {
            return true;
        });

        if (hit) {
            jTarget.css("cursor", cursorHoverStyle);
        } else {
            jTarget.css("cursor", "");
        }
    });

    let dropdown = $('#date-select');

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>--Please choose a Report Date--</option>');
    dropdown.prop('selectedIndex', 0);

    const url = '/api/reports/dates';

    // Populate dropdown with list of report dates
    $.getJSON(url, function (data) {
        $.each(data, function (key, entry) {
            dropdown.append($('<option></option>').attr('value', entry).text(entry));
        })
    });

    //original #date-select code
    // $('#date-select').change(function() {
    //     reportLayer.setSource(new ol.source.Vector({
    //         format: new ol.format.GeoJSON(),
    //         url: '/api/reports/' + $('#date-select').val()
    //     }));
    // });

    // original #search code
    // $('#search').click(function() {
    //     reportLayer.setSource(new ol.source.Vector({
    //         format: new ol.format.GeoJSON(),
    //         url: '/api/reports/search?q=' + $('#input').val()
    //     }))
    // });

        var vectorSource = new ol.source.Vector();
        // var vector = new ol.layer.Vector({
        //     source: vectorSource,
        //     style: new ol.style.Style({
        //         stroke: new ol.style.Stroke({
        //             color: 'rgba(0, 0, 255, 1.0)',
        //             width: 2
        //         })
        //     })
        // });

    // generate a GetFeature request for #date-select
    $('#date-select').change(function() {
        const requestDateFeature = new ol.format.WFS().writeGetFeature({
            srsName: 'EPSG:3857',
            featureNS: 'http://launchcode.org',
            featurePrefix: 'zika',
            featureTypes: ['states_with_cases_by_date'],
            outputFormat: 'application/json',
            filter: new ol.format.filter.EqualTo('report_date', dropdown.val())

        });

        fetch('http://3.17.135.191:8989/geoserver/zika/wfs', {
            method: 'POST',
            body: new XMLSerializer().serializeToString(requestDateFeature)
        }).then(function(response) {
            return response.json();
        }).then(function(json) {
            var features = new ol.format.GeoJSON().readFeatures(json);
            stuff = {};
            for(let i = 0; i < features.length; i++) {
                if(features[i].get('state_id') in stuff) {
                    stuff[features[i].get('state_id')] += features[i].get('cases');
                } else {
                    stuff[features[i].get('state_id')] = features[i].get('cases');
                }
            }
            // console.log(stuff);
            // console.log(features);
            for(let i = 0; i < features.length; i++) {
                features[i]['values_']['state_total'] = stuff[features[i].get('state_id')];
            }
            // console.log(features);
            vectorSource = new ol.source.Vector();
            vectorSource.addFeatures(features);
            reportLayer.setSource(vectorSource);
            map.getView().fit(vectorSource.getExtent());
        });
    });

    //generate a GetFeature request for #search
    $('#search').click(function() {
        const searchRequestFeature = new ol.format.WFS().writeGetFeature({
            srsName: 'EPSG:3857',
            featureNS: 'http://launchcode.org',
            featurePrefix: 'zika',
            featureTypes: ['states_with_cases_by_date'],
            outputFormat: 'application/json',
            filter: new ol.format.filter.EqualTo('name_0', $('#input').val()),
        });

        // Start trying sort order

        const OGCNS = "http://www.opengis.net/ogc";
        const DOCUMENT = document.implementation.createDocument(null, null);

        // create the sort order XML node
        let sortByNode = DOCUMENT.createElementNS(OGCNS, "SortBy");
        let sortPropertyNode = DOCUMENT.createElementNS(OGCNS, "SortProperty");
        let propertyNameNode = DOCUMENT.createElementNS(OGCNS, "PropertyName");
        propertyNameNode.textContent = "report_date";
        let sortOrderNode = DOCUMENT.createElementNS(OGCNS, "SortOrder");
        sortOrderNode.textContent = "ASC";

        sortPropertyNode.appendChild(propertyNameNode);
        sortPropertyNode.appendChild(sortOrderNode);
        sortByNode.appendChild(sortPropertyNode);

        // append the sort XML node to the <Query> (searchRequestFeature)
        searchRequestFeature.firstChild.appendChild(sortByNode);

        //  End trying sort order

        fetch('http://3.17.135.191:8989/geoserver/zika/wfs', {
            method: 'POST',
            body: new XMLSerializer().serializeToString(searchRequestFeature)
        }).then(function(response) {
            return response.json();
        }).then(function(json) {
            var features = new ol.format.GeoJSON().readFeatures(json);
            stuff = {};
            for(let i = 0; i < features.length; i++) {
                if(features[i].get('state_id') in stuff) {
                    stuff[features[i].get('state_id')] += features[i].get('cases');
                } else {
                    stuff[features[i].get('state_id')] = features[i].get('cases');
                }
            }
            // console.log(stuff);
            // console.log(features);
            for(let i = 0; i < features.length; i++) {
                features[i]['values_']['state_total'] = stuff[features[i].get('state_id')];
            }
            console.log(features);
            vectorSource = new ol.source.Vector();
            vectorSource.addFeatures(features);
            for(let i = 0; i < vectorSource.length; i++) {
                vectorSource.sort(function(a,b) {
                    a = vectorSource[i]['values_']['report_date'].split('-').reverse().join('');
                    b = vectorSource[i]['values_']['report_date'].split('-').reverse().join('');
                    return a > b ? 1 : a < b ? -1 : 0;
                    // return a.localeCompare(b);         // <-- alternative
                })};
            reportLayer.setSource(vectorSource);
            map.getView().fit(vectorSource.getExtent());
        });
    });

});
